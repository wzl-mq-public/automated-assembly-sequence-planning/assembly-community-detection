# Graph Clustering 
Graph clustering (or community detection) describes a number of techniques for identifying connected components in a graph.
They usually use a measure of centrality for nodes (for which there are many suggested definitions in literature) to identify
important "central" nodes, or use edge cutting to identify weak connections within the graph. Graphs of n nodes are usually represented
by adjacency matrices A (n x n), where each element Aij represents the connection between nodes i and j. If Aij = 0, then there is no 
edge from i to j. In an unweighted digraph, Aij = Aji and Aij = 1 iff there is an edge between i and j. In a weighted graph Aij != 0 if 
there is an edge between the nodes. In an undirected graph Aij = Aji does not (always) hold. 

## Idea
Preparing the STEP file naturally yields several potential candidate matrices that can be seen as adjacency matrices of a graph corresponding to 
the input model:
- **Contact Matrix:** The contact matrix for an input model with n shapes is a n x n matrix where Aij = 1 if shapes i and j are in contact. It is interesting
to consider since graph clusters are assumed to be groups of parts with strong connectivity (e.g. many parts in a cluster are in contact with each other). Thus
it is reasonable to assume that they should be consider as one subassembly. 
- **Norms Matrix:** The norms matrix for an input model with n shapes is a n x n matrix where Aij is equal to a norm of the distance of shape i to shape j in 
euclidian space. It is a symmetrical matrix along the diagonal with Aii = 0 for all i and thus represents a fully connected graph excluding self loops. It
is intersting to consider this graph/matrix sincce objects that are closer together have smaller edge weights between them and thus are likely to be grouped
together by a clustering algorithm.
- **Weighted Contact Matrix:** This matrix combines the two above by taking the contact matrix and replacing every Aij with Aij = 1  from the contact matrix with
the corresponding entry from the norms matrix. Thus the resulting graph has edge weights corresponding to the distances in euclidian space and edges corresponding
to actual connectivity.

## Approach
Two graph clustering algorithms were implemented and can be used by the user: Garvin-Newman and Louvain. Garvin-Newman relies on cutting edges to identify connected 
components (edge-betweenness centrality) wheras Louvain relies on comparing the number of links within in a community compared to the number of expected 
links between nodes in the overall graph.

## Results (Preliminary)
Preliminary results show that the input for the clustering algorithm should be chosen according to the selected STEP model. 
Different models yield better separation into connected components with different input matrices. 
This suggests, that the best approach would be to provide visual representations of the segmentation results and then let the 
user choose the appropriate suggestion.
