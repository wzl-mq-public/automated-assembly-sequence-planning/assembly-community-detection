"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" This file provides functionality to compute a contact matrix from a STEP file
" (C) 2021 - Nehel Malhotra, Karim El Zaatari - InVEST 2021/22 
" MIT License 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

import numpy as np
import pandas as pd
import itertools
from OCC.Core.BRepMesh import BRepMesh_IncrementalMesh
from OCC.Core.BRepExtrema import BRepExtrema_ShapeProximity,BRepExtrema_ShapeList,BRepExtrema_TriangleSet
from OCC.Extend.ShapeFactory import translate_shp
from OCC.Core.gp import gp_Pnt, gp_Vec

#write a contact matrix as a csv file
def write_contact_matrix(matrix, names, filename):
    contacts = cm_transpose_add(matrix)
#    df_contact = pd.DataFrame(contacts,index=names,columns=names)
    df_contact = pd.DataFrame(contacts)
    df_contact.to_csv(filename)

# Added by Lucas 
def write_norms_matrix(matrix, names, filename):
    contacts = cm_transpose_add(matrix)
#    df_contact = pd.DataFrame(contacts,index=names,columns=names)
    df_contact = pd.DataFrame(contacts)
    df_contact.to_csv(filename)
# Added by Lucas 
def write_cm_with_norms_matrix(matrix, names, filename):
    contacts = cm_transpose_add(matrix)
#    df_contact = pd.DataFrame(contacts,index=names,columns=names)
    df_contact = pd.DataFrame(contacts)
    df_contact.to_csv(filename)

def cm_transpose_add(df_array):
    df_array = df_array + df_array.T - np.diag(np.diag(df_array))
    return df_array

def cm_proximity(shapes_object,make_csv=False):
    names = []
    for shape in shapes_object:
        label, _ = shapes_object[shape]
        names.append(label)

    print("Meshing ", len(shapes_object), " shapes")
    i = 0
    for shape in shapes_object:
        BRepMesh_IncrementalMesh(shape, 1.0)
        print("progress: ",  (i/len(shapes_object)) * 100, "%")
        i+=1

    print("Computing contact matrix")
    contacts = np.zeros(shape=[len(names),len(names)])
    for i, j in itertools.combinations(np.arange(len(list(shapes_object.keys()))), 2):
        shape_a = list(shapes_object.keys())[i]
        shape_b = list(shapes_object.keys())[j]
        proximity = BRepExtrema_ShapeProximity(shape_a, shape_b)
        proximity.Perform()
        shapes_in_contact_a = []
        shapes_in_contact_b = []
        print("progress: ",  ((i*len(shapes_object)+j) / (len(shapes_object)**2))*100 , "%")
        if proximity.IsDone():
            subs1 = proximity.OverlapSubShapes1().Keys()

            subs2 = proximity.OverlapSubShapes2().Keys()

            for sa in subs1:
                temp = translate_shp(proximity.GetSubShape1(sa), gp_Vec(0, 0, 5))
                shapes_in_contact_a.append(temp)
            for sa in subs2:
                temp = translate_shp(proximity.GetSubShape2(sa), gp_Vec(0, 0, 5))
                shapes_in_contact_b.append(temp)
            if len(shapes_in_contact_a) and len(shapes_in_contact_b):
                contacts[i,j] = 1

    return contacts, names