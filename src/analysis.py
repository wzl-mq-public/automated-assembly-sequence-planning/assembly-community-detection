"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" This file provides functionality to perform different visual data analysis operations
" (C) 2021 - Nehel Malhotra, Karim El Zaatari - InVEST 2021/22 
" MIT License 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

# two-dimensional PCA of a set of n-dimensional vectors
def pca_2(data):
    pca = PCA(2)
    projected = pca.fit_transform(data)
    plt.scatter(projected[:, 0], projected[:, 1], alpha=0.3)
    #plt.xlabel('component 1')
    #plt.ylabel('component 2')
    plt.colorbar()
    plt.show()