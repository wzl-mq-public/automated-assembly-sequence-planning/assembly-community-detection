"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" Goes through the step_files directory and performs the preprocessing on all objects
" (C) 2021 - Daniel Swoboda <swoboda@kbsg.rwth-aachen.de> - InVEST 2021/22 
" MIT License 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

import util
import process
from os import listdir
from os.path import isfile, join

path = "../data/step_files"

files = [f[:-4] for f in listdir(path) if isfile(join(path, f)) and ".stp" in f]

for file in files:
    print("Working on ", file)
    util.read_or_gen_p_shapes(path="../data/step_files/", name=file, override=True)
    util.read_or_gen_p_cm(path="../data/step_files/", name=file, override=True)
    util.read_or_gen_p_cog(path="../data/step_files/", name=file, override=True)