"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" Renders a serialized set of shapes from a generated pickle
" (C) 2021 - Daniel Swoboda <swoboda@kbsg.rwth-aachen.de> - InVEST 2021/22 
" MIT License 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

from util import *
from gui import *
from OCC.Display.SimpleGui import init_display

path = "../data/step_files/"
file = "vertical_drop_lift"

shapes_names_colors, list_of_shapes = read_or_gen_p_shapes(path, file)

display, start_display, add_menu, add_function_to_menu = init_display()
gui_import_as_multiple_shapes(display, shapes_names_colors)
start_display()