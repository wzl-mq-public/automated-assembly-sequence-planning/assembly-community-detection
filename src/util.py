"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" This file provides utilities to process STEP files for usage with PythonOCC
" (C) 2021 - Daniel Swoboda <swoboda@kbsg.rwth-aachen.de> - InVEST 2021/22
" MIT License
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

from OCC.Core.GProp import GProp_GProps
from OCC.Core.BRepGProp import brepgprop_VolumeProperties
from OCC.Core.Quantity import Quantity_Color, Quantity_TOC_RGB
from OCC.Extend.DataExchange import read_step_file_with_names_colors
from OCC.Core.Bnd import Bnd_Box
from OCC.Core.BRepMesh import BRepMesh_IncrementalMesh
from OCC.Core.BRepBndLib import brepbndlib_Add
from OCC.Core.BRep import BRep_Tool, BRep_Builder
from OCC.Core.BRepAlgoAPI import BRepAlgoAPI_Fuse
from OCC.Extend.DataExchange import write_stl_file
from OCC.Core.StlAPI import stlapi_Read, StlAPI_Writer
from OCC.Core.TopoDS import (
    topods,
    TopoDS_Wire,
    TopoDS_Vertex,
    TopoDS_Edge,
    TopoDS_Face,
    TopoDS_Shell,
    TopoDS_Solid,
    TopoDS_Shape,
    TopoDS_Compound,
    TopoDS_CompSolid,
    topods_Edge,
    topods_Vertex,
    TopoDS_Iterator,
)
from OCC.Extend.TopologyUtils import WireExplorer, TopologyExplorer
import numpy as np
import shutil
import random
import os

import contact

# converts values r,g,b in [0-1] to Quantity_Color object for rendering
def rgb_color(r, g, b):
    return Quantity_Color(r, g, b, Quantity_TOC_RGB)

# converts values r,g,b in [0-1] to Quantity_Color object for rendering
def rgb_color(ct):
    return Quantity_Color(ct[0], ct[1], ct[2], Quantity_TOC_RGB)

def rgb_color_random():
    return Quantity_Color(random.random(), random.random(), random.random(), Quantity_TOC_RGB)

# returns the mass of a shape assuming uniform density
def get_mass(props:GProp_GProps, shape):
    brepgprop_VolumeProperties(shape, props)
    return props.Mass()

# returns the center of gravity of a shape assuming uniform density distribution
def get_center_of_gravity(props:GProp_GProps, shape):
    brepgprop_VolumeProperties(shape, props)
    return props.CentreOfMass().Coord()

def get_boundingbox(shape, tol=1e-6, use_mesh=True):
    bbox = Bnd_Box()
    bbox.SetGap(tol)
    if use_mesh:
        mesh = BRepMesh_IncrementalMesh()
        mesh.SetParallelDefault(True)
        mesh.SetShape(shape)
        mesh.Perform()
        if not mesh.IsDone():
            raise AssertionError("Mesh not done.")
    brepbndlib_Add(shape, bbox, use_mesh)

    xmin, ymin, zmin, xmax, ymax, zmax = bbox.Get()
    return xmin, ymin, zmin, xmax, ymax, zmax, xmax-xmin, ymax-ymin, zmax-zmin

#given a list of tuples, combine two pairs if they have a mutual element
#iteratively until no more tuples can be combined.
# e.g. (2,1) (2,3) -> (1,2,3) and (2,4) (4,7) -> (2,4,7)
# ----> (1,2,3) (2,4,7) -> (1,2,3,4,7)
def combine_pairs(groups):
    out = []
    while len(groups) > 0:
        first, *rest = groups
        first = set(first)

        lf = -1
        while len(first) > lf:
            lf = len(first)

            rest2 = []
            for r in rest:
                if len(first.intersection(set(r))) > 0:
                    first |= set(r)
                else:
                    rest2.append(r)
            rest = rest2

        out.append(first)
        groups = rest
    return out

# computes the norms for the centers of gravity to determine the distances
def compute_norms(shape_idx: int, coords:np.array):
    distances = coords - coords[shape_idx]
    return np.linalg.norm(distances, axis=-1)

#combines given list of shapes into a TopoDS_Compound
def combine_shapes(shapes):
    list_contains_null_shape = True
    compound = TopoDS_Compound()
    builder = BRep_Builder()
    builder.MakeCompound(compound)
    for shape in shapes:
        if shape.IsNull():
            list_contains_null_shape = False
            continue
        builder.Add(compound, shape)
    return compound, list_contains_null_shape

#combines given list of shapes into a TopoDS_Compound
def combine_shapes_from_indexlist(shapes, indexlist):
    list_contains_null_shape = True
    compound = TopoDS_Compound()
    builder = BRep_Builder()
    builder.MakeCompound(compound)
    for index in indexlist:
        if shapes[index].IsNull():
            list_contains_null_shape = False
            continue
        builder.Add(compound, shapes[index])
    return compound

#combine given list of shapes into a single TopoDS_Solid using the BRep Fuse algorithm
def combine_shapes_iteratively(shapes):
    shape = shapes[0]
    list_contains_null_shape = True
    if len(shapes) > 1:
        for add_shape in shapes[1:]:
            if add_shape.IsNull():
                list_contains_null_shape = False
                continue
            shape = BRepAlgoAPI_Fuse(shape, add_shape).Shape()
    return shape, list_contains_null_shape

def export_shapes_to_stl(dirpath, shapes):
    if os.path.isdir(dirpath):
        shutil.rmtree(dirpath)
    os.mkdir(dirpath, 0o777)
    os.chdir(dirpath)

    for i in range(0,len(shapes)):
        print("Writing", str(i)+".stl", i+1, "of", len(shapes))
        write_stl_file(shapes[i],  str(i)+".stl")

    os.chdir("..")

def remove_stl_export_dir(dirpath):
    if os.path.isdir(dirpath):
        shutil.rmtree(dirpath)


#this export function assumes that the dictionary keeps its order (as of assertion)
#the assumption holds for every python version since 2019.
def export_tree_to_stl_recursive(level, dirpath, tree, exporter):
    os.mkdir(dirpath, 0o777)
    for i in range(0, len(tree["data"])):
        shape_data =  tree["data"][list(tree["data"].keys())[i]]
        shape_name = shape_data[0]
        shape_color = shape_data[1]
        shape_duplicates = shape_data[2]
        shape = list(tree["data"].keys())[i]
        #export
        os.chdir(dirpath)
        exporter.Write(shape, str(i)+".stl")

        if shape_duplicates is not None and len(shape_duplicates) > 0:
            os.mkdir(dirpath+"/"+str(i)+"-duplicates", 0o777)
            os.chdir(dirpath+"/"+str(i)+"-duplicates")
            for duplicate in shape_duplicates:
                exporter.Write(duplicate, str(shape_duplicates.index(duplicate))+".stl")
            os.chdir("..")
        if tree["children"] is not None:
            shape_node = tree["children"][i]
            export_tree_to_stl_recursive(level+1, dirpath+"/"+str(i), shape_node, exporter) 


def export_tree_to_stl(dirpath, tree):
    exporter = StlAPI_Writer()
    exporter.SetASCIIMode(False)
    export_tree_to_stl_recursive(0, dirpath+"-root", tree, exporter)
    