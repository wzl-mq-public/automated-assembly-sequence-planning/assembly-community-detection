"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" Provides functions to do community detection on different graph inputs akin to adjacency matrices
" (C) 2021 - Karim El Zaatari <arimzaatary@gmail.com - InVEST 2021/22
" MIT License
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

import networkx as nx
import community as community_louvain

def edge_to_remove(graph):
    G_dict = nx.edge_betweenness_centrality(graph)
    edge = ()

    # extract the edge with the highest betweenness centrality
    for key, value in sorted (G_dict.items(), key = lambda item: item[1], reverse= True):
        edge = key
        break
    return edge

#returns result of girvan_newman community detection as list of lists with
#each list being a community of shapes
def girvan_newman(graph):
    # find number of connected components
    sg = nx.connected_components(graph)
    sg_counts = nx.number_connected_components(graph)
    while (sg_counts == 1):
        graph.remove_edge(edge_to_remove(graph)[0],edge_to_remove(graph)[1])
        sg = nx.connected_components(graph)
        sg_counts = nx.number_connected_components(graph)

    #girvan newman
    node_groups = []
    for i in sg:
        node_groups.append(list(i))
    return node_groups

#returns result of louvain community detection as list of lists with
#each list being a community of shapes
def louvain(graph):
    community = community_louvain.best_partition(graph)

    community_dict = {}
    for shape_idx in community:
        if community[shape_idx] not in community_dict.keys():
            community_dict[community[shape_idx]] = [shape_idx]
        else:
            community_dict[community[shape_idx]].append(shape_idx)
    return list(community_dict.values())


def generate_nx_graph(m):
    cm_graph = nx.Graph()
    for id in range (len(m)):
        cm_graph.add_node(id)
    for i in range (len(m)):
        for j in range (len(m)):
            if m[i][j] != 0:
                cm_graph.add_edge(i,j,weight = m[i][j])
    return cm_graph
