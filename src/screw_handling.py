from OCC.Extend.DataExchange import write_step_file
from util import *
from gui import *
from OCC.Display.SimpleGui import init_display

#this defines a simple on-click callback that prints shape information
def gui_simple_callback(selected, *kwargs):
    for shape in selected:
        if shape in list_of_shapes:
            # remove item whose key correponds to shape from shape_name_colors
            del shapes_names_colors[shape]
            # remove "shape" from list_of_shapes
            list_of_shapes.remove(shape)
            # update  rendering 
            gui_import_as_multiple_shapes(display, shapes_names_colors)


def filter_items(shapes_names_colors, items_to_remove): 
    new_shapes_names_colors = {}
    for key in shapes_names_colors:
        if shapes_names_colors[key][0] not in items_to_remove:
            new_shapes_names_colors[key] = shapes_names_colors[key]

    return new_shapes_names_colors, list(new_shapes_names_colors.keys())
    

path = "../data/step_files/"
file = input("Please enter the file you want to edit: ")
output_file = input("Please enter the file name you want to save to: ")
items_to_remove = input("Please enter a list of names (comma separated) for items you want to remove: ").split(",")

#read the step file
shapes_names_colors, list_of_shapes = read_or_gen_p_shapes(path, file)

#filter by name
shapes_names_colors, list_of_shapes = filter_items(shapes_names_colors, items_to_remove)

        
display, start_display, add_menu, add_function_to_menu = init_display()
gui_import_as_multiple_shapes(display, shapes_names_colors)


display.register_select_callback(gui_simple_callback)

input("Press any key to save the result: ")

for item in shapes_names_colors:
    write_step_file(item, path+output_file+".stp", application_protocol="AP203")

