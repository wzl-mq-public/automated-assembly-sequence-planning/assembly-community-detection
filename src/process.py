"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" This file provides utility functions for the project
" (C) 2021 - Daniel Swoboda <swoboda@kbsg.rwth-aachen.de> - InVEST 2021/22 
" MIT License 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import random
import itertools
from sklearn.cluster import DBSCAN # <- Machine Learning comes here
from sklearn.cluster import OPTICS # <- Machine Learning comes here
from sklearn.decomposition import PCA
import argparse

from OCC.Core.GProp import GProp_GProps
from OCC.Core.BRepGProp import brepgprop_VolumeProperties
from OCC.Core.Quantity import Quantity_Color, Quantity_TOC_RGB
from OCC.Display.SimpleGui import init_display
from OCC.Extend.DataExchange import read_step_file_with_names_colors
from OCC.Core.Bnd import Bnd_Box
from OCC.Core.BRepMesh import BRepMesh_IncrementalMesh
from OCC.Core.BRepBndLib import brepbndlib_Add
from OCC.Core.BRepExtrema import BRepExtrema_ShapeProximity,BRepExtrema_ShapeList,BRepExtrema_TriangleSet
from OCC.Extend.ShapeFactory import translate_shp
from OCC.Core.gp import gp_Pnt, gp_Vec

import pickle as p
import contact
from util import *

# get the shapes and shapes with names and colors from a step file
def get_shapes(filepath):
    snc = read_step_file_with_names_colors(filepath)
    list_of_shapes = list(snc.keys())

    shapes_names_colors = {}
    #convert to rgb tuple to allow storage
    for shape in list_of_shapes:
        shapes_names_colors[shape] = (snc[shape][0],(snc[shape][1].Red(), snc[shape][1].Green(), snc[shape][1].Blue()))
    return shapes_names_colors, list_of_shapes

# gets the vertices that make up the mesh of the given shape
def get_vertices(shape):
    te = TopologyExplorer(shape)
    vertices = []
    for vertex in te.vertices():
        x = BRep_Tool.Pnt(vertex).X()
        y = BRep_Tool.Pnt(vertex).Y()
        z = BRep_Tool.Pnt(vertex).Z()
        vertices.append([x,y,z])
    return vertices

def process_gen_cm(shapes_names_colors):
    return contact.cm_proximity(shapes_names_colors)

def process_write(object, filepath):
    f = open(filepath, 'wb')
    p.dump(object, f)
    f.close()

def process_load(filepath):
    f = open(filepath, "rb")
    object = p.load(f)
    f.close()
    return object

def path_name_to_path(path, name):
    if path[-1:] != "/":
        path_p_s = path + "/" + name + "_S.pickle"
        path_p_cm = path + "/" + name + "_CM.pickle"
        path_p_cog = path + "/" + name + "_COG.pickle"
        path_p_norms = path + "/" + name + "_NRM.pickle"
        path_p_vertices = path + "/" + name + "_VRT.pickle"
        path_stp = path + "/" + name + ".stp"
    else:
        path_p_s = path + name + "_S.pickle"
        path_p_cm = path + name + "_CM.pickle"
        path_p_cog = path + name + "_COG.pickle"
        path_p_norms = path + name + "_NRM.pickle"
        path_p_vertices = path + name + "_VRT.pickle"
        path_stp = path + name + ".stp"
    return path_p_s, path_p_cm, path_p_cog, path_p_norms, path_p_vertices, path_stp

def read_or_gen_p_shapes(path, name, override = False):
    path_p_s, path_p_cm, path_p_cog, path_p_norms, path_p_vertices, path_stp = path_name_to_path(path, name)

    try:
        if override:
            raise Exception()
        shape_tuple = process_load(path_p_cm)
    except:
        print("Shapes not found, computing and serializing to pickle for future use!")
        shapes_names_colors, list_of_shapes = get_shapes(path_stp)
        process_write((shapes_names_colors, list_of_shapes), path_p_cm)
        shape_tuple = process_load(path_p_cm)

    shapes_names_colors = {}
    for shape in shape_tuple[0].keys():
        shapes_names_colors[shape] = [shape_tuple[0][shape][0],rgb_color(shape_tuple[0][shape][1])]

    return shapes_names_colors, shape_tuple[1]

# try to read a pickle file for the given name in the given path, if none exists, create the contact matrix and serialize it
def read_or_gen_p_cm(path, name, shapes_names_colors = None, override = False):
    path_p_s, path_p_cm, path_p_cog, path_p_norms, path_p_vertices, path_stp= path_name_to_path(path, name)

    try:
        if override:
            raise Exception()
        return process_load(filepath=path_p_s)
    except:
        print("Contact matrix not found, computing and serializing to pickle for future use!")
        if shapes_names_colors is None:
            shapes_names_colors, list_of_shapes = get_shapes(path_stp)
        process_write(contact.cm_proximity(shapes_names_colors), path_p_s)
        return process_load(filepath=path_p_s)

# try to read a pickle file for the given name in the given path, if none exists, create the COG array
def read_or_gen_p_cog(path, name, shapes=None, props=None, override=False):
    path_p_s, path_p_cm, path_p_cog, path_p_norms, path_p_vertices, path_stp = path_name_to_path(path, name)

    try:
        if override:
            raise Exception()
        return process_load(filepath=path_p_cog)
    except:
        print("COGs not found, computing and serializing to pickle for future use!")
        if shapes is None:
            shapes_names_colors, shapes = read_or_gen_p_shapes(path, name)
            props = GProp_GProps()
        cogs = np.array([get_center_of_gravity(props, shape) for shape in shapes])
        process_write(cogs, path_p_cog)
        return process_load(filepath=path_p_cog)

# try to read a pickle file for the given name in the given path, if none exists, create the COG array
def read_or_gen_p_norms(path, name, cogs=None, override=False):
    path_p_s, path_p_cm, path_p_cog, path_p_norms, path_p_vertices, path_stp = path_name_to_path(path, name)

    try:
        if override:
            raise Exception()
        return process_load(filepath=path_p_norms)
    except:
        print("COGs not found, computing and serializing to pickle for future use!")
        if cogs is None:
            cogs = read_or_gen_p_cog(path, name)
        norms = np.array([compute_norms(idx, cogs) for idx in range(len(cogs))])
        process_write(norms, path_p_norms)
        return process_load(filepath=path_p_norms)


# try to read a pickle file for the given name in the given path, if none exists, create the vertices array
def read_or_gen_p_vertices(path, name, shapes=None, override=False):
    path_p_s, path_p_cm, path_p_cog, path_p_norms, path_p_vertices, path_stp = path_name_to_path(path, name)

    try:
        if override:
            raise Exception()
        return process_load(filepath=path_p_vertices)
    except:
        print("Vertices not found, computing and serializing to pickle for future use!")
        if shapes is None:
            shapes_names_colors, shapes = read_or_gen_p_shapes(path, name, override = False)
        vertices = np.array([get_vertices(shape) for shape in shapes])
        process_write(vertices, path_p_vertices)
        return process_load(filepath=path_p_vertices)