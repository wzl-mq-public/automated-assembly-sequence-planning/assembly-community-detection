"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" This file provides PyOCC GUI utility functions for the project
" (C) 2021 - Daniel Swoboda <swoboda@kbsg.rwth-aachen.de> - InVEST 2021/22 
" MIT License 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

from OCC.Core.Quantity import Quantity_Color, Quantity_TOC_RGB
import random

#import shapes for displaying
def gui_import_as_multiple_shapes(display, shapes_names_colors, event=None):
    display.EraseAll()
    for shpt_lbl_color in shapes_names_colors:
        label, c = shapes_names_colors[shpt_lbl_color][:2]
        display.DisplayColoredShape(shpt_lbl_color, color=c)
    display.FitAll()

def gui_import_as_multiple_shapes_webGL(my_ren, shapes_names_colors, event=None):
    for shpt_lbl_color in shapes_names_colors:
        label, c = shapes_names_colors[shpt_lbl_color][:2]
        print("label", label, "c", c)

        #r = random.random()
        #g = random.random()
        #b = random.random()
        #c = (r, g, b)


        #todo:change to proper color setting before this random thingy

        my_ren.DisplayShape(shpt_lbl_color, color=c, mesh_quality=0.5)
    my_ren.render()