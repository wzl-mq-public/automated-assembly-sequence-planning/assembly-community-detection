"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" This file provides clustering functionality for distances based histgorams
" (C) 2021 - Daniel Swoboda <swoboda@kbsg.rwth-aachen.de> - InVEST 2021/22
" MIT License
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
import numpy as np
import random
import analysis
import matplotlib.pyplot as plt
from sklearn.cluster import OPTICS # <- Machine Learning comes here

from util import *

# computes a histogram out of the given array with bins of size step_size and cutoff at max_dist
def cl_compute_histogram(norms:np.ndarray, step_size, max_dist):
    hist, edges = np.histogram(norms, bins=np.arange(0, step=step_size, stop=max_dist-(step_size/2)), range=(0, max_dist))
    return hist #/ (np.arange(step_size, step=step_size, stop=max_dist-(step_size/2)) ** distance_scaledown)

def cl_compute_max_norm(points):
    max_dist = max(
        np.linalg.norm(points[from_idx] - points[to_idx]) for from_idx in range(0, len(points)) for to_idx in
        range(0, from_idx)) / 2
    return max_dist

# compute the max distance between to shapes to discretize the space of distances correctly
def cl_compute_step_max(cogs,steps):
    max_dist = cl_compute_max_norm(cogs)
    step_size = max_dist / steps
    # compute the histograms
    return max_dist, step_size

# clusters the shapes based on histograms that represent the neighborhood of each shape
# in a discretized way. Returns a cluster dictionary, having an array of shape indices per cluster,
# the raw clustering result and the histograms
def cl_compute_clusters(histograms, min_samples=2,eps=0.5):
    #clustering = DBSCAN(eps=eps, min_samples=min_samples).fit(histograms)  # UPDATE THE EPS VALUE BASED ON SIZE OF OBJECT
    clustering = OPTICS(min_samples=min_samples).fit(histograms)  # UPDATE THE EPS VALUE BASED ON SIZE OF OBJECT

    cluster_dict = {}
    for idx in range(len(clustering.labels_)):
        if clustering.labels_[idx] in cluster_dict:
            cluster_dict[clustering.labels_[idx]].append(idx)
        else:
            cluster_dict[clustering.labels_[idx]] = [idx]
    return cluster_dict, clustering

def compute_histogram_mesh(vertice, cog):
    steps = 50
    max_dist, step_size = cl_compute_step_max(np.append(vertice,np.array([cog]),axis=0),steps=steps)#50 works well
    distances = vertice - cog
    norms = np.linalg.norm(distances, axis=-1)
    histogram = np.zeros(steps-1)
    if step_size != 0.0:
        histogram = cl_compute_histogram(norms, step_size, max_dist)
    return histogram

def compute_histogram_neighbors(cogs, cog):
    steps = 10
    distances = cogs - cog
    norm = np.linalg.norm(distances, axis=-1)
    max_dist, step_size = cl_compute_step_max(cogs, steps=steps)
    return cl_compute_histogram(norm, step_size, max_dist)

