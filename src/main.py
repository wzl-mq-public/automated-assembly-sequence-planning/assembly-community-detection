"""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" (C) 2021 - InVEST 2021/22
" MIT License
"""""""""""""""""""""""""""""""""""""""""""""

from sklearn.decomposition import PCA
from numpy import sign
import argparse
import collections
import analysis
import copy
import pprint
import better_exchook
import networkx as nx
import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import networkx as nx
import community as community_louvain

from OCC.Core.GProp import GProp_GProps
from OCC.Core.Quantity import Quantity_Color, Quantity_TOC_RGB
from OCC.Display.SimpleGui import init_display
from OCC.Extend.TopologyUtils import WireExplorer, TopologyExplorer
from datetime import datetime

from util import *
from contact import *
from clustering import *
from gui import *
from communities import *
from disassembly import *
from process import *

# better debugging
better_exchook.install()

# configure
visual_confirmation = True
export = False
experimental = True
community_algorithm = "Louvain" #"GM" for Girvan-Newman, or "Louvain" for Louvain.
histogram_algorithm = "mesh" #"mesh" for a mesh-based approach, "neihbors" for a neihborhood-based approach.
visualisation = "production" #"demo" to show every step, "production" to only show states in which the HITL interacts.
autoconfirm = True
autoconfirm_threshold = 5
path = "../data/step_files/"
#file = "delta_robot"
file = "centrifugal_pump"
#file = "cylinder_radial_engine"
#file = "IoC_canopy"
#file = "vertical_drop_lift"
#file = "tower_crane"


filename_cm = "U:\Git_Projects\graph-clustering-tools\cm_"+file+".csv"


#%%

"""
Python OCC GUI Code
"""
#this defines a simple on-click callback that prints shape information
def gui_info_callback(selected, *kwargs):
    global neighbors_matrix, matrix, wl_labels, clustering
    for shape in selected:
        if shape in shapes:
            i = shapes.index(shape)
            print("Selected shape", i)
          
displayed_communities = 0  
def gui_community_monitor_switcher_callback(selected, *kwargs):
    global display,partitions_colored,displayed_communities

    gui_import_as_multiple_shapes(display, partitions_colored[displayed_communities])
    displayed_communities+=1

    if displayed_communities == len(partitions_colored):
        displayed_communities = 0

def gui_none_callback(selected, *kwargs):
    return
#
def viz_two_layered(display, shapes, sets, info_text=None, input_prompt=None):
    if not info_text is None:
        print(info_text)
    colored_shapes = {}
    for set in sets:
        color = rgb_color_random()
        for shape in set:
            colored_shapes[shapes[shape]] = ["", color]
    gui_import_as_multiple_shapes(display, colored_shapes)
    if not input_prompt is None:
        input(input_prompt)
    return colored_shapes

def viz_three_layered(display, shapes, sets, info_text=None, input_prompt=None):
    if not info_text is None:
        print(info_text)
    colored_shapes = {}
    for group in sets:
        color = rgb_color_random()
        for combination in group:
            for shape in combination:
                colored_shapes[shapes[shape]] = ["", color]
    gui_import_as_multiple_shapes(display, colored_shapes)
    if not input_prompt is None:
        input(input_prompt)
    return colored_shapes

"""
Functionality
"""

# apply graph clustering on the input model to identify compounds of components
def graph_clustering(matrices_list, shapes, shapes_names_colors):
    global displayed_communities, partitions_colored
    
    print("Compute communities for",len(matrices_list),"input matrices")

    partitions_colored = []
    partitions_list = []
    graph_list = []
    for m in matrices_list:
        partitions_colored.append(dict())
        print("Round", len(partitions_colored), "of", len(matrices_list))
        #create copy of shapes_names_colors to set the colors according to the cluster
        for shape in shapes_names_colors:
            partitions_colored[len(partitions_colored) -1][shape] = shapes_names_colors[shape].copy()
        local_shapes_names_colors = partitions_colored[len(partitions_colored) -1]

        #create the graph object from the given adjacency matrix-style input matrix
        cm_graph = generate_nx_graph(m)
        graph_list.append(cm_graph)
        
        #compute the cluster
        communities = []
        if community_algorithm == "GM":
            communities = girvan_newman(cm_graph.copy())
        elif community_algorithm == "Louvain":
            communities = louvain(cm_graph.copy())
        partitions_list.append(communities)

        for community in communities:
            col = Quantity_Color(random.random(),random.random(),random.random(),Quantity_TOC_RGB)
            for item in community:
                local_shapes_names_colors[shapes[item]][1] = col

        print("Computed",len(communities), "clusters, with sizes",[len(x) for x in communities])

    #visual feedback for the selection
    gui_import_as_multiple_shapes(display, partitions_colored[displayed_communities])
    print("Showing displayed communities", displayed_communities)
    displayed_communities = displayed_communities + 1

    #register callback
    display.register_select_callback(gui_community_monitor_switcher_callback)
    input("Press enter to select the currently displayed partitions, click on the screen to change the partition")
    display.unregister_callback(gui_community_monitor_switcher_callback)
    display.register_select_callback(gui_info_callback)

    return partitions_list, graph_list

# use similarity between communities to identify potential mergers
def combine_communities(matrix, communities, graph):
    node_edges_count_dict = {}
    edges_to_community_dict = {k:[] for k in range(len(communities))}

    edge_count = 0
    for node in range(len(matrix)):
        for i in range(len( matrix [node])):
            edge_count = edge_count + graph.number_of_edges(node,i)
        node_edges_count_dict[node] = edge_count

    for index in range(len(matrix)):
        for group in range(len(communities)):
            if index in communities[group]:
                edges_to_community_dict[group].append(node_edges_count_dict[index]) 

    edges_to_community_list = list(edges_to_community_dict.values())
    maximum_value = max(list([item for sublist in edges_to_community_list for item in sublist]))

    histograms_similarity = []
    for i in range (len(edges_to_community_list)):
        histogram = cl_compute_histogram(np.array(edges_to_community_list[i]),1, maximum_value)
        histograms_similarity.append(histogram)

    clustering = OPTICS(min_samples= 2,metric='cosine').fit(histograms_similarity) 
    cluster_dict = {}
    for idx in range(len(clustering.labels_)):
        if clustering.labels_[idx] in cluster_dict:
            cluster_dict[clustering.labels_[idx]].append(idx)
        else:
            cluster_dict[clustering.labels_[idx]] = [idx]

    # take the clustering result and combine the communities that are in one cluster
    combined_communities = []
    for cluster in cluster_dict:
        combination = []
        for community in cluster_dict[cluster]:
            combination = combination+communities[community]
        combined_communities.append(combination)
    return combined_communities

# compute the histogram representation of the shapes
def compute_histogram_clusters(shapes, histogram_algorithm, vertices, cogs):
    histograms = []
    for i in range(0, len(shapes)):
        print('computing histogram', (i+1)/len(shapes))
        histogram = []
        if histogram_algorithm == "mesh":
            histogram = compute_histogram_mesh(vertices[i], cogs[i])
        elif histogram_algorithm == "neighbors":
            histogram = compute_histogram_neighbors(cogs, cogs[i])
        histograms.append(histogram)

    histograms = np.array(histograms)
    cluster_dict, clustering = cl_compute_clusters(histograms, 2)
    return cluster_dict, clustering

def compute_wl_labels(matrix, clustering):
    #build extended neighborhood matrix where each neighbor is marked by its cluster 
    neighbors_matrix = []
    for i in range(0, len(matrix)):
        neighbors = []
        for j in range(0, len(matrix[i])):
            if matrix[i][j] == 1:
                neighbors.append(clustering.labels_[j])
            else:
                neighbors.append(-2)
        neighbors_matrix.append(neighbors)

    #build first step weisfeiler-lehman node labels
    wl_labels = []
    for i in range(0, len(neighbors_matrix)):
        contact_cluster = np.array(neighbors_matrix[i])
        unique, counts = np.unique(contact_cluster, return_counts=True)
        wl_labels.append((unique, counts))
    return wl_labels
#
#build combinations based on wl-labels
def combine_shapes_same_labels(cluster_dict, wl_labels, matrix):
    #combine two shapes if all shapes in the same cluster have the same WL label
    pairs = []
    for cluster in cluster_dict:
        same_label = True
        prev_label = wl_labels[cluster_dict[cluster][0]]
        for elem in cluster_dict[cluster]:
            label = wl_labels[elem]
            if not np.array_equal(label[0],prev_label[0]):
                same_label = False

        #if all elements of the cluster got the same label
        #-> set pairs with all elements in contact
        if same_label:
            for elem in cluster_dict[cluster]:
                for partner in range(0, len(matrix[elem])):
                    if matrix[elem][partner]:
                        pairs.append((elem, partner))
    return pairs

#get a list of all the shapes handled here for later processing
def compute_unique_components(matrix, combined):
    unique_components = []
    for i in range(0, len(matrix)):
        occurs = False
        for pair in combined:
            if i in pair:
                occurs = True
        if not occurs:
            unique_components.append(i)
    return unique_components
#
#compute signature of combined shapes
def compute_compound_signatures(combined, clustering):
    combined_signatures = []
    signatures = []
    for shape_set in combined:
        shape_set_clusters = []
        for shape in shape_set:
            shape_set_clusters.append(clustering.labels_[shape])
        shape_set_clusters.sort()
        signature = np.unique(np.array(shape_set_clusters)).tolist()
        combined_signatures.append(signature)
        if not signature in signatures:
            signatures.append(signature)
    return combined_signatures, signatures

##identify duplicates based on signature
#def compute_duplicate_compounds(signatures, combined_signatures, combined):
#    combined_grouped = []
#    for signature in signatures:
#        group = []
#        for i in range(0, len(combined)):
#            if combined_signatures[i] == signature:
#                group.append(combined[i])
#        combined_grouped.append(group)
#    return combined_grouped
    
"""
Load the initial files
"""
# set up command line args
parser = argparse.ArgumentParser(description='Assembly Planning using ML')
parser.add_argument('-cm', type=bool, help='should the contact matrix be written to a file?',  required=False)
args = parser.parse_args()

# import the file
print("Loading shapes")
shapes_names_colors, shapes = read_or_gen_p_shapes(path, file)

# get props
props = GProp_GProps()

# get the contact matrix
print("Loading contact matrix")
matrix, names = read_or_gen_p_cm(path, file, shapes_names_colors = shapes_names_colors)

print(matrix)
write_contact_matrix(matrix,names,filename_cm)
    
# get centers of gravity
print("Loading COGs")
cogs = read_or_gen_p_cog(path, file, shapes = shapes, props = props)

# get distance norm matrix
print("Loading norms matrix")
norms = read_or_gen_p_norms(path, file, cogs = cogs)

# get the vertices matrix
print("Loading vertices matrix")
vertices = read_or_gen_p_vertices(path, file, shapes = shapes)

# compute distance-infused contact matrix
print("Computing distance-infused contact matrix")
cm_with_norms = matrix.copy()
for i in range (len(cm_with_norms)):
    for j in range(len(cm_with_norms[i])):
        if cm_with_norms [i][j] ==1:
            cm_with_norms [i][j] = norms [i][j]

#init the gui
display, start_display, add_menu, add_function_to_menu = init_display()

def build_partition_tree(matrix, norms, cm_with_norms, shapes, shapes_names_colors, vertices, cogs, display, level):
    displayed_communities = 0
    print("-----------------------------------------------------------")
    if autoconfirm and len(shapes) < autoconfirm_threshold:
        print("Autoconfirming - compound has less than threshold (", len(shapes),"/", autoconfirm_threshold,")!")
        return 

    print("Showing the current model at level", level)
    gui_import_as_multiple_shapes(display, shapes_names_colors)
    if input("Do you want to stop refinement at this step? (Y/N) ") in ["Y", "y"]:
        return None

    """
    Apply the configured graph clustering on the given input matrices
    Return a list of partitions of the components and a list of the generated networkx graph objects
    """
    partitions_list, graph_list = graph_clustering([matrix, norms, cm_with_norms], shapes, shapes_names_colors.copy())

    """
    Reduce the number of communities by combining similar ones, let user choose to take it or not
    """
    selected_partition = (displayed_communities - 1) % len(partitions_list)
    partition = partitions_list[selected_partition]

    print("Selected partition ",selected_partition)
    if len(partitions_list[selected_partition]) > 1:
        reduced_partition = combine_communities(matrix, partitions_list[selected_partition], graph_list[selected_partition])
        viz_two_layered(display, shapes, reduced_partition)

        # if the user decides not to use the combination, reset the result
        if input("Do you want to combine the communities as suggested? (Y/N) ") in ["Y","y"]:
            partition = reduced_partition
        else:
            viz_two_layered(display, shapes, partition)


    """
    Let user decide to export the result
    """
    print("Combining shapes based on selected partition",selected_partition)
    combined_shapes = {}
    for combination in partition:
        combined_shapes[combine_shapes_from_indexlist(shapes, combination)] = ["", rgb_color_random(), None]

    if input("Do you want to use the presented partition? (Y/N) ") in ["N", "n"]:
        """
        Compute histogram-based clusters to find similar components
        """
        print("Applying similar shape detection and combination")

        # compute the histogram representation of the shapes
        cluster_dict, clustering = compute_histogram_clusters(shapes, histogram_algorithm, vertices, cogs)

        #visual inspection
        if visualisation == "demo":
            viz_two_layered(display, shapes, [cluster_dict[key] for key in cluster_dict], "Showing identified similar shapes", "Press enter to continue ")

        # Identify redundant connected components, remove all but one representative

        #compute labels for nodes based on their neighborhoods
        wl_labels = compute_wl_labels(matrix, clustering)
        #build combinations based on wl-labels
        pairs = combine_shapes_same_labels(cluster_dict, wl_labels, matrix)
        #define connected regions similar to db scan if there is a path between them
        combined = combine_pairs(pairs)
        #get a list of all the shapes handled here for later processing
        unique_components = compute_unique_components(matrix, combined)

        #visual confirmation
        if visualisation == "demo":
            viz_two_layered(display, shapes, combined, "Press enter to show groups identified through WL", "Press enter to continue")

        # Identify duplicate component groups and remove n-1 of any duplicates

        #compute signature of combined shapes    
        combined_signatures, signatures = compute_compound_signatures(combined, clustering)
        #identify duplicates based on signature
        combined_grouped = compute_duplicate_compounds(signatures, combined_signatures, combined)

        #show the groups of detected duplicates
        if visualisation == "demo":
            viz_three_layered(display, shapes, combined_grouped, "Showing the identified duplicate groups (same group = same color)", "Press enter to show representative instances")

        #show only one instance per duplicate
        print("Showing the selected representative of each duplicate instance")
        combined_grouped_repr = combined_grouped.copy()
        duplicates = []
        for i in range(0,len(combined_grouped_repr)):
            duplicates.append(list(combined_grouped_repr[i])[1:])
            combined_grouped_repr[i] = combined_grouped_repr[i][0]
        grouped_shapes = viz_two_layered(display, shapes, combined_grouped_repr)
        print("---")
        print(duplicates)
        #add unique elements
        print("Showing the final segmentation")
        color = rgb_color_random()
        for shape in unique_components:
            grouped_shapes[shapes[shape]] = ["", color]
        gui_import_as_multiple_shapes(display, grouped_shapes)

        print("Combining the compounds")
        combined_shapes = {}
        for i in range(0, len(combined_grouped_repr)):
            combination = combined_grouped_repr[i]
            shape_duplicates = []
            for duplicate in duplicates[i]:
                print(duplicate)
                shape_duplicates.append(combine_shapes_from_indexlist(shapes, duplicate))
            combined_shapes[combine_shapes_from_indexlist(shapes, combination)] = ["", rgb_color_random(), shape_duplicates]
        for unique_component in unique_components:
            combined_shapes[shapes[unique_component]] = ["", rgb_color_random(), None]

        partition = [list(combined_group) for combined_group in combined_grouped_repr]+[[unique_component] for unique_component in unique_components]

        if input("Do you want to use the presented partition? (Y/N) ") in ["N", "n"]:
            print("None of the presented partitions were used, keeping model as collection of singular components!")
            return None


    print("Result of this step: ")
    gui_import_as_multiple_shapes(display, combined_shapes)
    print(" - achieved reduction in components:", len(combined_shapes.keys()), "of", len(shapes), "->",len(shapes)/len(combined_shapes.keys()))
    input("Press any key to go to the next iteration")
    
    children = []
    if len(partition) > 1:
        for part in partition:
            sub_matrix = [[matrix[i][j] for j in part] for i in part]
            sub_norms = [[norms[i][j] for j in part] for i in part]
            sub_cm_with_norms = [[cm_with_norms[i][j] for j in part] for i in part]
            sub_shapes = [shapes[i] for i in part]
            sub_shapes_names_colors = {}
            for shape in sub_shapes:
                sub_shapes_names_colors[shape] = shapes_names_colors[shape]
            sub_vertices = [vertices[i] for i in part]
            sub_cogs = [cogs[i] for i in part]

            child = build_partition_tree(sub_matrix, sub_norms, sub_cm_with_norms, sub_shapes, sub_shapes_names_colors, sub_vertices, sub_cogs, display, level+1)
            if child is not None:
                children.append(child)
            else:
                data = {}
                for shape in sub_shapes:
                    data[shape] = ["", rgb_color_random(), None]
                children.append({"children":None, "data":data})

    return {"children":children, "data":combined_shapes}

tree = build_partition_tree(matrix, norms, cm_with_norms, shapes, shapes_names_colors, vertices, cogs, display, 0)
if tree is None:
    print("User decided not to use a partition, build simple tree instead")
    data = {}
    for shape in shapes:
        data[shape] = ["", rgb_color_random(), None]
    tree = {"children":None, "data":data}


print("No unexplored subtree left, resulting tree: ")
pprint.pprint(tree)

#export_path = input("Please select a path to export the STL files to (enter for the current directory): ")
#if export_path == "":
#    export_path = os.getcwd()+"/export-"+datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
#    print("Entered empty string, using ", export_path)
#
#export_tree_to_stl(export_path, tree)
