import random

from sklearn.decomposition import PCA
from numpy import sign
import argparse
import collections
import analysis
import copy
import pprint
import better_exchook
import networkx as nx
import seaborn as sns; sns.set()

from gui import *
from communities import *
from disassembly import *
from process import *
from OCC.Display.WebGl import threejs_renderer
from OCC.Core.BRepPrimAPI import BRepPrimAPI_MakeTorus
from OCC.Core.gp import gp_Vec
from OCC.Extend.ShapeFactory import translate_shp

import pickle as pkl # added by Lucas

# configure
visual_confirmation = True
export = False
experimental = True
#community_algorithm = "Louvain" #"GM" for Girvan-Newman, or "Louvain" for Louvain.
community_algorithm = "Louvain" #"GM" for Girvan-Newman, or "Louvain" for Louvain.
histogram_algorithm = "mesh" #"mesh" for a mesh-based approach, "neihbors" for a neihborhood-based approach.
visualisation = "production" #"demo" to show every step, "production" to only show states in which the HITL interacts.
autoconfirm = True
autoconfirm_threshold = 5


# always change when needed
main_path="C:/Users/adam-uqef35nm77fzn5b/Documents/20_Dissertation/20_Tools/IoC_Scheduling/graph-clustering-tools"

# Find a way to optimize the import of files
import os

files = []   
path = "../data/step_files/"

files.append("cp_reduced")
#files.append("20230324_IoC_Demo")
#files.append("delta_robot")
#files.append("centrifugal_pump")
#files.append("cylinder_radial_engine")
#files.append("IoC_canopy")
#file = "vertical_drop_lift"
#file = "tower_crane"
file = files[0]

# creating the DF
result_data_0 = [
        ["VERIFICATION"],
        ["id"],
        ["product_name"],
        ["n_parts"],
        ["n_columns"],
        ["n_rows"],
        ["isolated_nodes"],
        ["n_isolates"],
        ["n_sub_graphs"],
        ["n_nodes_min_subgraph"],
        ["n_nodes_avg_subgraph"],
        ["n_nodes_max_subgraph"]
]

df_1 = pd.DataFrame(result_data_0)

list_matrices_names=['cm','norms','cm_with_norms']
#list_matrices_names=['cm_with_norms']
    
filename_cm = main_path+"/matrices/"+list_matrices_names[0]+file+".csv"
filename_norms = main_path+"/matrices/"+list_matrices_names[1]+file+".csv"
filename_cm_with_norms = main_path+"/matrices/"+list_matrices_names[2]+file+".csv"

#print("ex_rq2_"+str(cont))
   
"""
Load the initial files
"""
# set up command line args
parser = argparse.ArgumentParser(description='Assembly Planning using ML')
parser.add_argument('-cm', type=bool, help='should the contact matrix be written to a file?',  required=False)
args = parser.parse_args()

# import the file
print("Loading shapes")
shapes_names_colors, shapes = read_or_gen_p_shapes(path, file)

# get props
props = GProp_GProps()

# get the contact matrix
print("Loading contact matrix")
cm, names = read_or_gen_p_cm(path, file, shapes_names_colors = shapes_names_colors)
print(cm)

# Calculating fetures for the excel file
number_of_parts = len(shapes)
print("number_of_parts: ",number_of_parts)

number_of_columns = len(shapes)
print("number_of_columns: ",number_of_columns)

number_of_rows = len(shapes)
print("number_of_rows: ",number_of_rows)

# Other Matrices

# get centers of gravity
print("Loading COGs")
cogs = read_or_gen_p_cog(path, file, shapes = shapes, props = props)

# get distance norm matrix
print("Loading norms matrix")
norms = read_or_gen_p_norms(path, file, cogs = cogs)
#print(len(norms))

# get the vertices matrix
print("Loading vertices matrix")
vertices = read_or_gen_p_vertices(path, file, shapes = shapes)

# compute distance-infused contact matrix
print("Computing distance-infused contact matrix")
cm_with_norms = cm.copy()
for i in range (len(cm_with_norms)):
    for j in range(len(cm_with_norms[i])):
        if cm_with_norms [i][j] ==1:
            cm_with_norms [i][j] = norms [i][j]
            
matrices_list = []
matrices_list.append(cm)
matrices_list.append(norms)
matrices_list.append(cm_with_norms)

#%% creating dictionary for the names, indexes and matrices and extra info

thisdict = {
    "indexes" : np.arange(0,len(names)),
    "names" : names,
    "contact_matrix" : cm,
    "norms_matrix" : norms,
    "distance_infused_cm" : cm_with_norms,
    "number_of_parts" : number_of_parts,
    "number_of_columns" : number_of_columns,
    "number_of_rows" : number_of_rows,
}



with open(main_path+"/liasons_graphs/"+file+"/Extracted_Info_CAD_file_"+file+'.pkl', 'wb') as f:
    pkl.dump(thisdict, f)
    
#with open(main_path+"/liasons_graphs/"+file+"/Extracted_Info_CAD_file_"+file+'.pkl', 'rb') as f:
#    loaded_dict = pkl.load(f)
        
"""
# from the networkx (before Community detection)
cont=0

for matrix in matrices_list:
    
    #G = nx.from_pandas_adjacency(df)
    G = nx.from_numpy_matrix(matrix)
        
    isolated_nodes = list(nx.isolates(G))
    print("isolated_nodes: ",isolated_nodes)
    
    n_isolates = len(isolated_nodes)
    print("n_isolates: ",n_isolates)
    
    sub_graphs=list(G.subgraph(c) for c in nx.connected_components(G))
    n_sub_graphs = len(sub_graphs)
    print("n_sub_graphs: ",n_sub_graphs)
    
    sub_graphs_n_nodes = [] 
    for i, sg in enumerate(sub_graphs):
        print ("subgraph {} has {} nodes".format(i, sg.number_of_nodes()))
        sub_graphs_n_nodes.append(sg.number_of_nodes())
        #print ("\tNodes:", sg.nodes(data=True))
        #print ("\tEdges:", sg.edges())
        
    min_n_nodes = min(sub_graphs_n_nodes)
    print("min_n_nodes: ",min_n_nodes)
    max_n_nodes = max(sub_graphs_n_nodes)
    print("max_n_nodes: ",max_n_nodes)
    
    def Average(lst):
        return sum(lst) / len(lst)
    
    avg_n_nodes = Average(sub_graphs_n_nodes)
    print("avg_n_nodes: ",avg_n_nodes)
    
    
    #nx.draw(G, with_labels=False)
    #
    #plt.savefig(os.path.join(main_path+"/liasons_graphs/"+file+"/"+str(cont)+"_"+list_matrices_names[cont]+"_"+file))
    
    
    #
    plt.close()
    #plt.show()
    
    cont=cont+1
"""
#
#%% Eliminate isolates before community detection - reducing matrices - reduction based on CM
    
G = nx.from_numpy_matrix(cm)
isolated_nodes = list(nx.isolates(G))
#print(isolated_nodes)

cm_reduced = np.delete(cm, isolated_nodes, axis=0)
cm_reduced = np.delete(cm_reduced, isolated_nodes, axis=1)

norms_reduced = np.delete(norms, isolated_nodes, axis=0)
norms_reduced = np.delete(norms_reduced, isolated_nodes, axis=1)

cm_with_norms_reduced = np.delete(cm_with_norms, isolated_nodes, axis=0)
cm_with_norms_reduced = np.delete(cm_with_norms_reduced, isolated_nodes, axis=1)

matrices_list_reduced = []
matrices_list_reduced.append(cm_reduced)
matrices_list_reduced.append(norms_reduced)
matrices_list_reduced.append(cm_with_norms_reduced)


#%%
# functions for cimmunity detection


displayed_communities = 0  
def gui_community_monitor_switcher_callback(selected, *kwargs):
    global display,partitions_colored,displayed_communities

    gui_import_as_multiple_shapes(display, partitions_colored[displayed_communities])
    displayed_communities+=1

    if displayed_communities == len(partitions_colored):
        displayed_communities = 0



# apply graph clustering on the input model to identify compounds of components
def graph_clustering(matrices_list, shapes, shapes_names_colors):
    global displayed_communities, partitions_colored
    
    print("Compute communities for",len(matrices_list),"input matrices")

    partitions_colored = []
    partitions_list = []
    graph_list = []
    cont=0
    
    # control variables # added by Lucas
    number_communities_list = [] # Number of  communities
    average_size_communities = [] # Avg size of communities (number of nodes / per communities)
    std_size_communities = [] # Standard deviation of size of communities
    computing_time = []
    
    for m in matrices_list:
        program_starts = time.time() # added by Lucas
        
        partitions_colored.append(dict())
        print("Round", len(partitions_colored), "of", len(matrices_list))
        #create copy of shapes_names_colors to set the colors according to the cluster
        for shape in shapes_names_colors:
            partitions_colored[len(partitions_colored) -1][shape] = shapes_names_colors[shape].copy()
        local_shapes_names_colors = partitions_colored[len(partitions_colored) -1]

        #create the graph object from the given adjacency matrix-style input matrix
        cm_graph = generate_nx_graph(m)
        graph_list.append(cm_graph)

        #compute the cluster
        communities = []
        if community_algorithm == "GM":
            communities = girvan_newman(cm_graph.copy())
        elif community_algorithm == "Louvain":
            communities = louvain(cm_graph.copy())
        partitions_list.append(communities)

        for community in communities:
            #col = Quantity_Color(random.random(),random.random(),random.random(),Quantity_TOC_RGB)
            r = random.random()
            g = random.random()
            b = random.random()
            col = (r, g, b)
            for item in community:
                local_shapes_names_colors[shapes[item]][1] = col

        print("Computed",len(communities), "clusters, with sizes",[len(x) for x in communities])
       
        # Number of  communities
        number_communities_list.append(len(communities)) # added by Lucas
        # Avg size of communities (number of nodes / per communities)
        from statistics import mean # install and add to .ylm file
        average_size_communities.append(mean([len(x) for x in communities])) # added by Lucas
        #print('average_size_communities: ',average_size_communities)
        # Standard deviation of size of communities
        std_size_communities.append(np.std([len(x) for x in communities]))
        
        #visual feedback for the selection
        #gui_import_as_multiple_shapes(display, partitions_colored[displayed_communities])

        my_ren = threejs_renderer.ThreejsRenderer()
        gui_import_as_multiple_shapes_webGL(my_ren, partitions_colored[displayed_communities], event=None)
        my_ren.render()

        print("Showing displayed communities", displayed_communities)
        displayed_communities = displayed_communities + 1
        
        now = time.time() # added by Lucas
        print("It has been {0} seconds since the loop started".format(now - program_starts)) # added by Lucas
        computing_time.append(now - program_starts) # added by Lucas
        
        input("Press enter to select the currently displayed partitions, click on the screen to change the partition")

        import pyautogui # install and add to .ylm file
        im = pyautogui.screenshot(region=(325,125, 1025, 800))
        im.save(main_path+"/liasons_graphs/"+file+"/"+str(cont)+"_"+list_matrices_names[cont]+"_"+community_algorithm+"_"+file+"_screenshot.png")

        cont=cont+1


    return partitions_list, graph_list, number_communities_list, average_size_communities, std_size_communities, computing_time

#%%

#init the gui
#display, start_display, add_menu, add_function_to_menu = init_display()



from random import randint
color_list = []

for i in range(100):
    r = random.random()
    g = random.random()
    b = random.random()
    color_list.append((r,g,b))
    #color_list.append('#%06X' % randint(0, 0xFFFFFF))

partitions_list_reduced, graph_list, number_communities_list, average_size_communities, std_size_communities, computing_time = graph_clustering([cm_reduced, norms_reduced, cm_with_norms_reduced], shapes, shapes_names_colors.copy())
#partitions_list_reduced, graph_list, number_communities_list, average_size_communities, std_size_communities, computing_time = graph_clustering([cm_with_norms_reduced], shapes, shapes_names_colors.copy())


print("(#) Original number_of_parts: ",number_of_parts)
        
print("(#) Reduced number_of_parts: ", len(cm_reduced))

print("(#) number_communities_list: ",number_communities_list)

print("(#) average_size_communities: ",average_size_communities)
        
print("(#) std_size_communities: ",std_size_communities)

print("(#) computing_time_list: ",computing_time)


 # Saving DATA
 
list_matrices_names_with_algorithm=['cm_'+community_algorithm,'norms_'+community_algorithm,'cm_with_norms_'+community_algorithm]

list_file_names = [file,file,file]

original_number_of_parts_list = [number_of_parts,number_of_parts,number_of_parts]

reduced_number_of_parts_list = [len(cm_reduced),len(cm_reduced),len(cm_reduced)]

    
df = pd.DataFrame(data={"list_file_names" : list_file_names,"original_number_of_parts_list": original_number_of_parts_list, "reduced_number_of_parts_list" : reduced_number_of_parts_list,"Matrix": list_matrices_names_with_algorithm, "number_communities_list": number_communities_list, "average_size_communities": average_size_communities, "std_size_communities": std_size_communities, "computing_time_list": computing_time})
#df = pd.DataFrame(data={list_matrices_names_with_algorithm, number_communities_list, average_size_communities, std_size_communities, computing_time})
#df.set_index(list_matrices_names_with_algorithm)
df.to_csv(main_path+"/liasons_graphs/"+file+"/"+"Data_"+community_algorithm+"_community_detection"+"_"+file+'.csv', sep=',',index=False,header=True)
 
        
#print("partitions_list: ",partitions_list)

#print("len partitions_list[0]: ",partitions_list[0])

#print("len partitions_list[0][0]: ",partitions_list[0][0])

#print("len partitions_list[0][1]: ",partitions_list[0][1])

#print("len partitions_list[0][2]: ",partitions_list[0][2])

#print("graph_list: ",graph_list[0])


#quit()

cont=0;
cont2=0;

for matrix in matrices_list_reduced:

    
    fig = plt.figure()
    
    node_color = []
    
    matrix_array_index = np.arange(0,len(matrix))
    
    partitions_list_per_matrix = partitions_list_reduced[cont2]
    print(partitions_list_per_matrix)
    
    for node in matrix_array_index:
        cont=0;
        
        for partition in partitions_list_per_matrix:
            if node in matrix_array_index[partition]:
                node_color.append(color_list[cont])
            cont=cont+1
            #print(cont)
    
    print("Building resulting graph...")
    G = nx.from_numpy_matrix(matrix)
    nx.draw(G, node_color=node_color, with_labels=True, font_size=8, font_color="black", font_weight="bold")
    
    print("Dumping graph in pickle...")
    file_test = open(main_path+"/liasons_graphs/"+file+"/"+str(cont2)+"_"+list_matrices_names[cont2]+"_"+community_algorithm+"_community_detection"+"_"+file+'.mpl', 'wb')
    pkl.dump(fig, file_test)
    
    file_test = open(main_path+"/liasons_graphs/"+file+"/"+str(cont2)+"_"+list_matrices_names[cont2]+"_"+community_algorithm+"_community_detection"+"_"+file+'.mpl', 'rb')
    figure = pkl.load(file_test)
    #figure.show()
    
    #print(list_matrices_names[cont2])
    plt.savefig(os.path.join(main_path+"/liasons_graphs/"+file+"/"+str(cont2)+"_"+list_matrices_names[cont2]+"_"+community_algorithm+"_community_detection"+"_"+file))
    #
       
    plt.close()

    #plt.show()
    cont2=cont2+1

#%% Exclude isolates from matrices (cm and cm_with_norms), since norms has no isoloted nodes
   
'''    

cont=0;
cont2=0;

for matrix in matrices_list:
    
    node_color = []
    
    matrix_array_index = np.arange(0,len(matrix))
    
    partitions_list_per_matrix = partitions_list[cont2]
    #print("partitions_list_per_matrix: ",partitions_list_per_matrix)
    
    
    G = nx.from_numpy_matrix(matrix)
    
    print("List of isolates: ",list(nx.isolates(G)))
    
    #list(nx.isolates(G))
    
    for node in matrix_array_index:
        cont=0;
        
        for partition in partitions_list_per_matrix:
            if (node in matrix_array_index[partition]) and (node not in list(nx.isolates(G))) : #and (len(partition) > 1):
                node_color.append(color_list[cont])
            cont=cont+1
            #print(cont)
    #print("Tamanho do node_color: ",len(node_color))
    
    G.remove_nodes_from(list(nx.isolates(G)))
    
    #print("List of isolates",list(nx.isolates(G)))
    #print("isolates",nx.isolates(G))

    nx.draw(G, node_color=node_color, with_labels=True, font_size=8, font_color="black", font_weight="bold")

    #print(list_matrices_names[cont2])
    plt.savefig(os.path.join(main_path+"/liasons_graphs/"+file+"/3_"+file+"_"+ list_matrices_names[cont2]+"_community_detection"+"_"+community_algorithm+"_without_isolates"))
    #
    plt.show()
    cont2=cont2+1
'''
    
#%%

#    result_data = [
#        [" "],
#        ["ex_rq2_"+str(cont)],
#        [file],
#        [number_of_parts],
#        [number_of_columns],
#        [number_of_rows],
#        [isolated_nodes],
#        [n_isolates],
#        [n_sub_graphs],
#        [min_n_nodes],
#        [avg_n_nodes],
#        [max_n_nodes]
#    ]
#    
#    result_data_df = pd.DataFrame(result_data)
#
#    df_2 = result_data_df
#    
#    df_f = pd.concat([df_1,df_2], axis=1)
#    
#    df_1 = df_f
#
#print(df_f)
#
#product_name = "RQ2_experiment_log"
#df_f.to_excel(os.path.join(main_path+"/",product_name + "_graph_analysis_results.xlsx"),index=False,header=None)