"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assembly Planning using Machine Learning
" This file provides functionality to identify peripharal shapes in a model
" (C) 2021 - Nehel Malhotra, Karim El Zaatari - InVEST 2021/22 
" MIT License 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
from OCC.Core.GProp import GProp_GProps
from OCC.Core.Quantity import Quantity_Color, Quantity_TOC_RGB
from OCC.Display.SimpleGui import init_display
from OCC.Core.BRepAlgoAPI import BRepAlgoAPI_Fuse
from OCC.Core.TopTools import TopTools_ListOfShape

import argparse

from util import *
from contact import *
from clustering import *
from gui import *
import analysis

"""
Python OCC GUI Code
"""
displayed_communities = 0

#this defines a simple on-click callback that prints shape information
def gui_simple_callback(selected, *kwargs):
    global display,shapes_list,displayed_communities

    gui_import_as_multiple_shapes(display, shapes_list[displayed_communities])
    print("I am showing the displayed community",displayed_communities)
    displayed_communities+=1
    
    if displayed_communities == len(shapes_list):
        displayed_communities = 0
    
            
"""
Support Functions
"""
# function that filters items in contact with a specific shape and checks distance to COG of fuse. The item is removed if it the farthest
# from fuse cog (compared to its neighbors)
def item_eliminate(index):
    contact_list =[]
    for ind in len(matrix):
        if matrix[index][ind] == 1:
            contact_list.append(norms_fuse_center[ind])
    if norms_fuse_center[index]> max(contact_list):
        shapes.remove(shapes[index])
    return shapes

# combine all components into a single item
def object_fuse(objects):
    fuse = objects[0]
    for shape in objects[1:]:
       fuse = BRepAlgoAPI_Fuse(fuse, shape).Shape()
    return fuse

"""
Main Logic
"""

path = "../data/step_files/"
file = input("Which file do you want to edit? ")

# import the file
print("Loading shapes")
shapes_names_colors, shapes = read_or_gen_p_shapes(path, file)
print(shapes)

# get props
props = GProp_GProps()

# get the contact matrix
print("Loading contact matrix")
matrix, names = read_or_gen_p_cm(path, file, shapes_names_colors = shapes_names_colors)

print("Computing COGs")
cogs = np.array([get_center_of_gravity(props, shape) for shape in shapes])

print("Computing COG Distances")
norms = np.array([cl_compute_norms_modified(idx, cogs) for idx in range(len(cogs))])

print("Compute center of gravity of fusion")
fuse = object_fuse(shapes)
center_gravity = np.array([get_center_of_gravity(props, fuse)])

print("compute distance between fuse and each item")
distances_fuse_center = cogs - center_gravity
norms_fuse_center = np.array(np.linalg.norm(distances_fuse_center, axis = -1))

index_farthest_element = np.argmax(norms_fuse_center)
print(norms_fuse_center)
print(index_farthest_element)
