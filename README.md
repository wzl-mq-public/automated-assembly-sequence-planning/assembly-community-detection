# Assembly Parts Clustering

## Description
The goal of this project is to split large CAD assembly models automatically into subassemblies using community detection algorithms.
This project contains scripts for:
- Creating different variations of contact matrices
- Different clustering algorithms (Louvain and Girvan-Newman) on these contact matrices


## Installation
Install utilizing Anaconda3 and [environment.yml](environment.yml).

## How to use the project
1. Load CAD Model in STEP format in [data/step_files](data/step_files/). A sample file "centrifugal_pump.stp" from [GrabCAD](https://grabcad.com/library/centrifugal-pump-41) is given.
2. Run the script [main.py](src/main.py)
3. Follow the instructions in the Python console.

## Acknowledgements
This work is part of the research project “Internet of Construction” that is funded by the Federal Ministry of Education and Research of Germany within the indirective on a joint funding initiative in the field of innovation for production, services and labor of tomorrow (funding number: 02P17D081) and supported by the project management agency “Projekttr¨ager Karlsruhe (PTKA)”. The authors are responsible for the content.

The code has been developed at [WZL of RWTH Aachen University](https://wzl.rwth-aachen.de)
Main authors of the code: Sören Münker, Daniel Swoboda, Karim El Zaatari, Nehel Malhotra, Lucas Manasses Pinheiro de Souza.
